﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ClassLibrary1
{
    public class Hapag315Parser
    {
        private readonly List<string[]> _ediData;
        private string _senderId;
        private string _reciverId;
        private int _currentElement;

        public Hapag315Parser(string ediData)
        {
            _ediData = ediData.Split('~')
                .Select((line,index)=>$"{index}*{line}".Split('*'))
                .ToList();
            _senderId = _ediData.Single(x => x[1] == "ISA")[7];
            _reciverId = _ediData.Single(x => x[1] == "ISA")[9];
        }

        public IEnumerable<ReleaseData> parseData()
        {

            var retval = parseISA(_ediData);


            return retval;
        }

        public IEnumerable<ReleaseData> parseISA(List<string[]> items)
        {
            var retval = new List<ReleaseData>();

            var gsStarts = items.Where(x => x[1] == "ISA").Select(x => x[0]).ToArray();
            foreach (var gsIndex in gsStarts)
            {
                var section = items.SkipWhile(x => x[0] != gsIndex).TakeWhile(x => x[1] != "IEA").ToList();
                retval.AddRange(parseGS(section));
            }

            return retval;
        }

        public IEnumerable<ReleaseData> parseGS(List<string[]> items)
        {
            var retval = new List<ReleaseData>();

            var gsStarts = items.Where(x => x[1] == "GS").Select(x => x[0]).ToArray();
            foreach (var gsIndex in gsStarts)
            {
                var section = items.SkipWhile(x => x[0] != gsIndex).TakeWhile(x => x[1] != "GE").ToList();
                retval.AddRange(parseST(section));
            }

            return retval;
        }

        public IEnumerable<ReleaseData> parseST(List<string[]> items)
        {
            var retval = new List<ReleaseData>();

            var stStarts = items.Where(x => x[1] == "ST").Select(x => x[0]);
            foreach (var stIndex in stStarts)
            {
                var section = items.SkipWhile(x => x[0] != stIndex).TakeWhile(x => x[1] != "SE").ToArray();
                retval.Add(parseSection(section));
            }

            return retval;
        }

        public ReleaseData parseSection(IEnumerable<string[]> section)
        {
            
            var b4Line = GetLine(section, "B4");
            var q2Line = GetLine(section, "Q2");
            var r4Line = GetLine(section, "R4");
            var retval = new ReleaseData();

            retval.SenderId = _senderId;
            retval.ReciverId = _reciverId;
            retval.ShipmentStatus = GetShipmentStatusName(b4Line[4]);
            retval.VesselCode = q2Line[2];
            retval.Voyage = q2Line[10];
            retval.Scac = q2Line[12];
            retval.Location = GetPortCodeName(r4Line[4]);
            retval.PortName = r4Line[5];
            retval.Equipment = b4Line[8] + b4Line[9];
            retval.DocumentNumber = section.Single(x => x[1] == "N9" && x[2] == "BM")[3];
            retval.LastFreeDate = section.Single(x => x[1] == "N9" && x[2] == "P8")[3];
            var eta = section.Single(x => x[1] == "DTM" && x[2] == "139");
            retval.ETA = DateTime.ParseExact($"{eta[3]} {eta[4]}", "yyyyMMdd HHmm", CultureInfo.InvariantCulture);

            return retval;
        }

        public string[] GetLine(IEnumerable<string[]> section, string segmentHeader)
        {
            return section.Single(x => x[1] == segmentHeader);
        }


        public string GetPortCodeName(string portCode)
        {
            var portCodes = new Dictionary<string, string>()
            {
                {"3512","OMAHA" },
                {"4501","KANSAS CITY" },
                {"5501","DALLAS" }
            };

            return portCodes[portCode]??portCode;
        }

        public string GetShipmentStatusName(string shipmentStatus)
        {
            var code = new Dictionary<string, string>()
            {
                {"CA","Shipment Cancelled" },
                {"CR","Carrier Release" },
                {"CU","Carrier and Customs Release" }
            };

            return code[shipmentStatus] ?? shipmentStatus;
        }


    }
}
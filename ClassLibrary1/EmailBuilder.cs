﻿using System.Linq;
using ClassLibrary1.Properties;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;

namespace ClassLibrary1
{
    public class EmailBuilder
    {
        public string GetEmailBody(ReleaseData[] releaseData)
        {
            var groupedItemsList = releaseData.GroupBy(r => new { r.ShipmentStatus, r.VesselCode, r.Voyage, r.Scac, r.Location, r.PortName, r.ETA, r.DocumentNumber, r.LastFreeDate })
                .Select(l => new
                {
                    l.Key.ShipmentStatus,
                    l.Key.VesselCode,
                    l.Key.Voyage,
                    l.Key.Scac,
                    l.Key.Location,
                    l.Key.PortName,
                    l.Key.ETA,
                    l.Key.DocumentNumber,
                    l.Key.LastFreeDate,
                    Equipment = l.Select(x => x.Equipment).Distinct().ToList()
                }).ToArray();
             

            var config = new TemplateServiceConfiguration { EncodedStringFactory = new RawStringFactory() };

            var service = new TemplateService(config);
            Razor.SetTemplateService(service);

            var template = Resources.EmailTemplate;
            Razor.Compile(template, null, "templateKey");
            var retval = Razor.Run("templateKey", new{Releases = groupedItemsList.ToArray()});
            return retval;

        }
    }
}
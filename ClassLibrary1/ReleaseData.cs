﻿using System;

namespace ClassLibrary1
{
    public class ReleaseData
    {
        public DateTime ETA;
        public string ShipmentStatus { get; set; }
        public string VesselCode { get; set; }
        public string Voyage { get; set; }
        public string Scac { get; set; }
        public string Location { get; set; }
        public string PortName { get; set; }
        public string Equipment { get; set; }
        public string LastFreeDate { get; set; }
        public string DocumentNumber { get; set; }
        public string ReciverId { get; set; }
        public string SenderId { get; set; }
    }
}